.SUFFIXES:

default:
	@cat Makefile | grep '^[a-z]*:' | sed 's/:.*//'

CFLAGS = -std=c89 -Wall -Wextra -nodefaultlibs -nostartfiles -ffunction-sections -fdata-sections -static

LDFLAGS = -Wl,--gc-sections -T link.ld



# elf

elf: shboot.elf
shboot.elf: shboot.S link.ld
	gcc -o shboot.elf ${CFLAGS} ${LDFLAGS} shboot.S

bin: shboot.bin
shboot.bin: shboot.elf
	objcopy -O binary shboot.elf shboot.bin

viewelf: shboot.elf
	readelf -a shboot.elf | less

viewbin: shboot.bin
	hexdump -C shboot.bin | less

viewasm: shboot.elf
	objdump -D shboot.elf | less



# disk

img: disk.img
disk.img:
	qemu-img create disk.img 100M
	@echo format disk as gpt with one big partition
	echo -e "g\nn\n\n\n\nw" | fdisk disk.img
	#limine bios-install disk.img 1

flash: shboot.bin disk.img
	dd if=shboot.bin of=disk.img conv=notrunc

viewimg: disk.img
	hexdump -C disk.img | less



# emulator

run: flash
	@echo 'ctrl+a x' to quit
	qemu-system-x86_64 -nographic -hda disk.img

gdb: flash
	@echo launch gdb
	@echo (gdb) target remote localhost:1234
	@echo (gdb) layout asm
	qemu-system-x86_64 -nographic -hda disk.img -s -S
