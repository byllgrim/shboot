/* Function Numbers */
#define  FN_WCHAR  $0x0E

.global  _start
_start:
    mov  FN_WCHAR,  %ah  /* spec:bios_interrupt_table */

    mov  $'s',  %al
    int  $0x10  /* spec:bios_interrupt_call */

    mov  $'h',  %al
    int  $0x10

    mov  $'i',  %al
    int  $0x10

    mov  $'t',  %al
    int  $0x10
